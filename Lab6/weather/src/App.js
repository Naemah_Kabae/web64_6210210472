import logo from './logo.svg';
import './App.css';
import {Box, AppBar,Typography, Toolbar,Card } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import {useEffect, useState} from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import YouTube from 'react-youtube';


function App() {

  const [ temperature, setTemperature] = useState();
  const weatherAPIBaseUrl=
            "https://api.openweathermap.org/data/2.5/weather?";
  const city = "Songkhla" ;
  const apikey ="4411577fe2402f809a7b0278d76b9bcf";

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '4411577fe2402f809a7b0278d76b9bcf',
    lat: '48.137154',
    lon: '11.576124',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });


  // open weather key 4411577fe2402f809a7b0278d76b9bcf
  useEffect( () => {
      setTemperature("-------");

    axios.get(weatherAPIBaseUrl+"q="+city+"&appid="+apikey).then ( (response)=> {
      let data = response.data;
      let temp = data.main.temp - 273 ;
      setTemperature(temp.toFixed(2));
    })
  }
  ,[] );

  return (
    <Box sx={{ flexGrow : 1, width :"100%" }}>
        <AppBar position ="static">
        <Toolbar>
          <Typography variat="h6">
            WeatherApp
          </Typography>
        </Toolbar>
       </AppBar>
    
    <Box sx = {{ justifyContent : 'center', marginTop :"20px",width : "100%",display:"flex"}}>
        <Typography variat="h4">
          อากาศหาดใหญ่วันนี้
        </Typography>
    </Box>
    <Box sx = {{ justifyContent : 'center', marginTop :"20px",width : "100%",display:"flex"}}>
        <Card sx={{ width: 275 }}>
                <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      อุณหภูมิหาดใหญ่วันนี้ คือ 
                    </Typography>
                    <Typography variant="h5" component="div">
                      {temperature}
                    </Typography>
                    <Typography variant="h5" component="div">
                    องศาเซลเซียส
                    </Typography>  
                  </CardContent>
        </Card>
        <ReactWeather
      isLoading={isLoading}
      errorMessage={errorMessage}
      data={data}
      lang="en"
      locationLabel="Munich"
      unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
      showForecast
    />
    </Box>
      <YouTube videoId="aG0YKCux4eI" />
    </Box> 
  
  );
}

export default App;
