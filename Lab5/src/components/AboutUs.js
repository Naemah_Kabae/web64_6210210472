import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { width } from '@mui/system';

function AboutUs (props) {

    return (
        <Box sx= {{ width:"60%"}}>
        <Paper elevation={3}>
           <h2> จัดทำโดย : {props.name} </h2>
           <h3> รหัสนักศึกษา {props.address} </h3>
           <h3> ภาควิชาวิทยาการคอมพิวเตอร์ {props.faculty} </h3>
        </Paper>
        </Box>
    );
}

export default AboutUs;
