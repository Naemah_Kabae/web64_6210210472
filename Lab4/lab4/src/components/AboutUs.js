
function AboutUs (props) {

    return (
        <div>
           <h2> จัดทำโดย : {props.name} </h2>
           <h3> รหัสนักศึกษา {props.address} </h3>
           <h3> ภาควิชาวิทยาการคอมพิวเตอร์ {props.faculty} </h3>
        </div>
    );
}

export default AboutUs;
