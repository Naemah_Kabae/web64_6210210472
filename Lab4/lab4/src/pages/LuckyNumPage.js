
import LuckyNumber from "../components/LuckyNumber";
import { useState } from "react";

function LuckyNumPage() {

    const [ number, setNumber ] = useState("");
    const [ result, setResult ] = useState(0);
    
    function Number() {
        let n = parseInt(number);
        let result = n;
        setResult(result);
        if (number == 29){
            setResult("ถูก เก่งมากเลยไอต้าว")
        }else {
            setResult("ผิด เดาใหม่เลย ฮึ")
        }
    }

    return(
        <div>
            กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99: <input type="text" 
                            value={number}
                            onChange={ (e) => { setNumber(e.target.value); } } /> <br /> 

            <button onClick={ ()=>{ Number() } }> ทายกันเลย </button>

            { result != 0 &&
                <div>
                    <hr />
                    <LuckyNumber 
                    result = {result} 
                    />
                </div>

            }
        </div>

    
    );
}

export default LuckyNumPage;